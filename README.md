# Custom EditText

#### 项目介绍
- 项目名称：Custom EditText
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个非常简单和轻量级的编辑框。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta 1
- 基线版本：Release 1.0.0

#### 效果演示

![screen1](https://images.gitee.com/uploads/images/2021/0723/113655_35c8430e_9260176.gif "屏幕截图.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```

allprojects {

    repositories {

         maven {
                  url 'https://s01.oss.sonatype.org/content/repositories/releases/'
              }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {

    implementation('com.gitee.chinasoft_ohos:CustomEditText:1.0.0')

    ......  

 }
 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
#### 使用说明

1.将仓库导入到本地仓库中

2.在布局文件中加入Custom EditText控件,代码实例如下:

```
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/apk/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:background_element="$color:colorPrimaryDark"
    ohos:orientation="vertical"
    ohos:padding="$float:_24vp">

    <com.lib.customedittext.CustomEditText
                ohos:height="match_content"
                ohos:width="match_parent"
                ohos:padding="$float:_1vp"
                app:edt_background="$color:transparent"
                app:edt_drawable_start="$graphic:ic_vpn_key_black"
                app:edt_hint="Enter Password"
                app:edt_imeOptions="2"
                app:edt_inputType="7"
                app:edt_maxLength="15"
                app:edt_padding="$float:_10vp"
                app:edt_password_toggle="true"
                app:edt_show_drawable="true"
                app:edt_text_color="$color:black"
                app:edt_text_size="$float:_18fp"/>

```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0
- 0.0.1-SNAPSHOT

#### 版权和许可信息

```
    Copyright 2019 RKMobile
    
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
    the License. You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
    an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
    specific language governing permissions and limitations under the License

```