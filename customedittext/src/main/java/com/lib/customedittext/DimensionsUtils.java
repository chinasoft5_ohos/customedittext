package com.lib.customedittext;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

/**
 * @author Kailash Chouhan
 * DimensionsUtils
 *
 * @since 2021.07.27
 */

public class DimensionsUtils {
    private DimensionsUtils() throws InstantiationException {
        throw new InstantiationException("This utility class is created for instantiation");
    }

    /**
     * getDimension
     *
     * @param attrSet 属性
     * @param name 名称
     * @param defaultValue 默认值
     * @return value
     */
    public static Integer getDimension(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getDimensionValue();
        }
        return value;
    }

    /**
     * getDimensionPixelSize
     *
     * @param context 上下文
     * @param integer 整数
     * @return i
     */
    public static int getDimensionPixelSize(Context context,  int integer) {
        int i = AttrHelper.fp2px(integer, context);
            return i;
    }

    /**
     * getFloatFromAttr
     *
     * @param attrSet 属性
     * @param name 名称
     * @param defaultValue 默认值
     * @return value
     */
    public static float getFloatFromAttr(AttrSet attrSet, String name, float defaultValue) {
        float value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getFloatValue();
        }
        return value;
    }

    /**
     * getElementFromAttr
     *
     * @param attrSet 属性
     * @param name 名称
     * @param defaultElement 默认值
     * @return value
     */
    public static Element getElementFromAttr(AttrSet attrSet, String name, Element defaultElement) {
        Element element = defaultElement;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            element = attrSet.getAttr(name).get().getElement();
        }
        return element;
    }

    /**
     * getBackgroundAttr
     *
     * @param attrSet 属性
     * @param name 名称
     * @param defaultElement 默认值
     * @return element
     */
    public static Element getBackgroundAttr(AttrSet attrSet, String name, Element defaultElement) {
        Element element = defaultElement;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            element = attrSet.getAttr(name).get().getElement();
            if(element==null){
             int value =  attrSet.getAttr(name).get().getColorValue().getValue();
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(RgbColor.fromArgbInt(value));
                element = shapeElement;
            }
        }
        return element;
    }

    /**
     * getColorFromAttr
     *
     * @param attrSet 属性
     * @param name 名称
     * @param defaultValue 默认值
     * @return value
     */
    public static int getColorFromAttr(AttrSet attrSet, String name, int defaultValue) {
        int value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getColorValue().getValue();

        }
        return value;
    }

    /**
     * getBooleanFromAttr
     *
     * @param attrSet 属性
     * @param name 名称
     * @param defaultValue 默认值
     * @return value
     */
    public static boolean getBooleanFromAttr(AttrSet attrSet, String name, boolean defaultValue) {
        boolean value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getBoolValue();
        }
        return value;
    }

    /**
     * getIntegerFromAttr
     *
     * @param attrSet 属性
     * @param name 名称
     * @param defaultValue 默认值
     * @return value
     */
    public static Integer getIntegerFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getIntegerValue();
        }
        return value;
    }

    /**
     * getStringFromAttr
     *
     * @param attrSet 属性
     * @param name 名称
     * @param defaultValue 默认值
     * @return value
     */
    public static String getStringFromAttr(AttrSet attrSet, String name, String defaultValue) {
        String value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getStringValue();
        }
        return value;
    }
}
